/*! \file  sketch.c
 *
 *  \brief  Blink an LED, Arduino style
 *
 *  This sketch relies on some functions (defined in Arduino.h) which
 *  translate Arduino pin numbers into PIC ports.  In addition, there
 *  is a (hidden) main that calls the setup() and loop() functions, as
 *  well as providing the configuration bits.  Arduino.h also defines
 *  the Arduino delay() function in terms of the builtin __delay32.
 *
 *  \author jjmcd
 *  \date March 19, 2013, 10:06 AM
 *
 * Software License Agreement
 * Copyright (c) 2013 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the file COPYING for a complete description.
 */

#include "../include/Arduino.h"

int nPotValue;

/*! setup() - Initialization for the sketch */
/*! Set pin 8 (Orange LED) to be an output
 *  Set pin 13 (pushbutton) to be an input
 *  Pin 9 (high intensity orange) does not need to be set
 */
void setup()
{
    pinMode( 8, OUTPUT );
    pinMode( 13, INPUT );
}

/*! loop() - Main body of the sketch */
/*! The shield has two LEDs on pins 8 and 9, and a button on pin 13.
 *  The sketch blinks the LED on pin 8.  If the button is held down,
 *  the LED on pin 8 stops blinking.  The LED on pin 9 has its
 *  brightness adjusted according to the pot position.
 */
void loop()
{
    /* Set the left LED depending on the pot value */
    nPotValue = analogRead( A3 );
    /* Note that analogRead returns 0-1023, analogWrite
     * only 0-255 */
    analogWrite( 9, nPotValue/4 );

    /* Blink the right LED unless the button is pressed*/
    digitalWrite( 8, !digitalRead( 13 ) );
    delay(100);
    digitalWrite( 8, HIGH );
    delay(100);
}
