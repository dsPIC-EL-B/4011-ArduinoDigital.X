/*! \file  ArduinoMain.c
 *
 *  \brief Mainline for Arduino-style sketches
 *
 *
 *  \author jjmcd
 *  \date March 19, 2013, 9:41 AM
 *
 * Software License Agreement
 * Copyright (c) 2013 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the file COPYING for a complete description.
 */

#include <xc.h>

// Configuration fuses
//! Use crystal oscillator, 16X PLL for 29.5 MIPS clock
_FOSC (XT_PLL16);
//! Watchdog timer off
_FWDT (WDT_OFF);
//! 16ms Power on timer, 2.7V brownout detect, enable MCLR
_FBORPOR (PWRT_16 & BORV27 & MCLR_EN);
//! No code protection
_FGS( GWRP_OFF & CODE_PROT_OFF );


void setup( void );
void loop( void);

/*! main - Mainline for Arduino-style sketches */
/*! Simply calls the user-supplied setup() function, then
 *  in an endless loop, calls loop();
 *
 */
int main(int argc, char *argv[])
{
    setup();
    while ( 1 )
    {
        loop();
    }
    return 0;
}
